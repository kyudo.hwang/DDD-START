FROM ubuntu:latest
LABEL authors="khwang"

# Install Java 17
RUN apt-get update && apt-get install -y openjdk-17-jdk


COPY target/*.jar /app.jar

# Run the JAR file
CMD ["java", "-jar", "/app.jar"]